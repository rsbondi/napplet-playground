import {relayPool, getPublicKey} from 'nostr-tools'

// you can start fresh generate random keys https://gobittest.appspot.com/PrivateKey
const privkeys = [
  "5acf6938fb40560e6571d69ad2e57784485fa5f2d0578d412652f2038699a712",
  "178013e005aad307b006014d7436f7f738d209c50d4eb340608605f58c6457c8",
  "6e9a242f1774500ff26162b4858f2ed0a55636684e4342ff021478c615048f1a"
]



const pubkeys = privkeys.map(k => getPublicKey(k))
let keyindex = 0

let appEvents = []
const ul = document.querySelector('ul')
const note = document.querySelector('#note')
const identity = document.querySelector('#identity')
const pre = document.querySelector('pre')
const appId = document.querySelector('#appId')
const appCode = document.querySelector('#appCode')
const load = document.querySelector('#load')
const deployForm = document.querySelector('#deployForm')
const toggle = document.querySelector('#toggle')
const setup = document.querySelector('#setup')
const run = document.querySelector('#run')
const embedded = document.querySelector('#embedded')
const relayInput = document.querySelector('#relayInput')
let mode = 0
const pool = relayPool()

let relays
const config = localStorage.getItem('relays')
if (config) {
  relays = JSON.parse(config)
} else relays = ['ws://localhost:7447'] // default to local on default port of go relay

relayInput.value = relays.join(', ')

async function goNostr() {
  pool.setPrivateKey(privkeys[keyindex])
  await Promise.all(relays.map(r => pool.addRelay(r, {read: true, write: true})))

  function onevent(event, relay) {
    
    if ((event.kind === 99 || event.kind === 98) && !~appEvents.map(ev => ev.id).indexOf(event.id)) {
      // console.log(`got an event from ${relay.url} which is already validated.`, event)
      let appKind 
      if (event.tags.length) {
        if (event.tags[0][0] === 'c') appKind = 'D'; else appKind = 'P'
      } else appKind = 'R'
      const li = document.createElement('li')
      li.textContent = `${appKind} ${event.pubkey.slice(0,8)}... ${new Date(parseInt(event.created_at+'000', 10)).toString().slice(4,25)} ${event.content}`
      li.id = event.id
      li.className = `action${appKind}`
      if (appEvents.length) ul.insertBefore(li, ul.firstChild); else ul.appendChild(li);
      
      appEvents.push(event)
      
      if (appKind === 'D') {
        appCode.value=""
      }

    }
  }

  ul.addEventListener('click', e => {
    const event = appEvents.find(item => item.id === e.target.id)
    pre.textContent = JSON.stringify(event, null, 2)
    if (!event.tags.length) {
      appId.value = event.id
    }

  })
  
  await pool.sub({filter: [{authors: pubkeys}, {kind: 99}, {kind: 98}], cb: onevent})

  pubkeys.forEach(f => {
    const option = document.createElement('option')
    option.textContent = `${f.slice(0,8)}...`
    identity.appendChild(option)
  })

  identity.addEventListener('change', () => {
    keyindex = identity.selectedIndex
    pool.setPrivateKey(privkeys[keyindex])
  })

  

  document.querySelector('#registerForm').addEventListener('submit', async e => {
    e.preventDefault()
    if (note.value) {
      let evt = await pool.publish({
        pubkey:pubkeys[keyindex],
        created_at: Math.round(new Date().getTime() / 1000),
        kind: 99,
        content: note.value
      }, (status, url) => {
        if (status === 1) {
          console.log('publish registration', url, evt)
          note.value = ""
        }
      })
    }
  })

  deployForm.addEventListener('submit', async e => {
    e.preventDefault()
    if (appId.value && appCode.value) {
      let evt = await pool.publish({
        pubkey:pubkeys[keyindex],
        created_at: Math.round(new Date().getTime() / 1000),
        kind: 99,
        content: `deployment ${appEvents.find(evt => evt.id === appId.value).content}`,
        tags: [
          ["c", appId.value, appCode.value]
        ]
      }, (status, url) => {
        if (status === 0) {
          console.log('publish code', url, evt)
        }
        if (status === 1) {
          console.log('publish code', url, evt)
          note.value = ""
        }
      })
    }
  })

  function loadApp() {
    // pool.reqEvent({id: appId.value})
    // TODO: reqEvent, this shortcut since I deployed, need to just from the id
    const event = appEvents.find(item => {
      return item.tags.length && item.tags[0][0] === "c" && item.tags[0][1] === appId.value
    })
    appCode.value = event.tags[0][2]
    embedded.srcdoc = event.tags[0][2]

    if(mode === 0) toggleMode()
  }

  load.addEventListener('click', loadApp)

  relayInput.addEventListener('change', async () => {
    const newRelays = relayInput.value.split(',').map(r => r.trim())
    for (var r=relays.length-1; ~r; r--) {
      const relay = relays[r]
      if (!~newRelays.indexOf(relay)) {
        await pool.removeRelay(relay)
        relays.splice(r, 1)
      }
    }

    await newRelays.forEach(async r => {
      if (!~relays.indexOf(r)) {
        await pool.addRelay(r, {read: true, write: true})
        relays.push(r)
        localStorage.setItem('relays', JSON.stringify(relays))
      }
    })
  })
}

goNostr()

toggle.addEventListener('click', toggleMode)

function toggleMode() {
  if (mode === 0) {
    run.className = ""
    setup.className = "hide"
  } else {
    run.className = "hide"
    setup.className = ""
  }
  mode = 1 - mode
}

window.addEventListener("message", (event) => {
  if (event.origin === window.location.href.replace(/\/$/, ''))
    console.log('nostr client received event',event.data)
    if (event.data.type === 'publish') {
      const nostrEvent = ({...event.data.event})
      nostrEvent.pubkey = pubkeys[keyindex]
      nostrEvent.created_at = Math.round(new Date().getTime() / 1000)

      pool.publish(nostrEvent).then(console.log)      
    }

    if (event.data.type ===  'ready') {
      embedded.contentWindow.postMessage({
        type: 'init', 
        app:appId.value,
        user: pubkeys[keyindex],
        events: appEvents.filter(e => e.kind === 98 && e.pubkey === pubkeys[keyindex] && e.tags[0][1] === appId.value)
      })      
    }
}, false);